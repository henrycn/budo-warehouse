package org.budo.warehouse.web.impl;

import javax.annotation.Resource;

import org.budo.warehouse.logic.api.DataProducerLoader;
import org.budo.warehouse.web.api.DataProducerApi;
import org.springframework.stereotype.Component;

/**
 * @author lmw
 */
@Component
public class DataProducerApiImpl implements DataProducerApi {
    @Resource
    private DataProducerLoader dataProducerLoader;

    @Override
    public void loadDataProducer() {
        dataProducerLoader.loadDataProducer();
    }

    @Override
    public void restartThread() {
        dataProducerLoader.restartThread();
    }
}