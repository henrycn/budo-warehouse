package org.budo.warehouse.logic.bean;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.budo.dubbo.protocol.async.config.BudoAsyncReferenceBean;
import org.budo.dubbo.protocol.async.config.BudoAsyncServiceBean;
import org.budo.dubbo.protocol.async.repository.activemq.ActiveMQAsyncRepository;
import org.budo.dubbo.protocol.async.repository.kafka.KafkaAsyncRepository;
import org.budo.support.javax.sql.util.JdbcUtil;
import org.budo.support.lang.util.MapUtil;
import org.budo.support.spring.bean.factory.support.BeanBuilder;
import org.budo.support.spring.util.SpringUtil;
import org.budo.warehouse.logic.api.DataConsumer;
import org.budo.warehouse.logic.consumer.DataEntryPojoConsumer;
import org.budo.warehouse.logic.consumer.jdbc.JdbcDataConsumer;
import org.budo.warehouse.logic.consumer.jdbc.MysqlDataConsumer;
import org.budo.warehouse.logic.consumer.mail.MailDataConsumer;
import org.budo.warehouse.logic.producer.WarehouseCanalMessageHandler;
import org.budo.warehouse.logic.util.PipelineUtil;
import org.budo.warehouse.service.api.IDataNodeService;
import org.budo.warehouse.service.entity.DataNode;
import org.budo.warehouse.service.entity.Pipeline;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * @author limingwei
 */
@Slf4j
@Component
public class LogicDynamicBeanProvider {
    @Resource
    private ApplicationContext applicationContext;

    @Resource
    private IDataNodeService dataNodeService;

    public DataConsumer dataConsumer(Pipeline pipeline) {
        DataNode targetDataNode = dataNodeService.findByIdCached(pipeline.getTargetDataNodeId());
        String url = targetDataNode.getUrl();

        if (url.startsWith("jdbc:")) {
            return this.jdbcDataConsumer(pipeline);
        }

        if (url.startsWith("async:")) { // kafka:192.168.4.253:9092 activemq:tcp://192.168.4.251:61616
            DataConsumer asyncDataConsumer = this.asyncDataConsumer(pipeline);
            return new DataEntryPojoConsumer(asyncDataConsumer, pipeline);
        }

        if (url.startsWith("mail")) { // 邮件警告
            return this.mailDataConsumer(pipeline);
        }

        throw new RuntimeException("#49 targetDataNode=" + targetDataNode);
    }

    private String canalMessageHandler(DataNode dataNode) {
        String beanId = "canalMessageHandler-" + dataNode.getId() + "-" + dataNode.getUrl();

        if (this.applicationContext.containsBean(beanId)) {
            log.warn("#81 bean exists, return, beanId=" + beanId);
            return beanId;
        }

        new BeanBuilder() //
                .id(beanId) //
                .type(WarehouseCanalMessageHandler.class) //
                .property("dataNodeId", dataNode.getId() + "") //
                .registerTo(this.applicationContext);

        return beanId;
    }

    public void canalDataProducer(DataNode dataNode) {
        String canalMessageHandlerId = this.canalMessageHandler(dataNode);

        // 作为 destination, 作为 zk path, 的规则限制
        String beanId = "canalDataProducer-" + dataNode.getId() + "-" + dataNode.getUrl();
        beanId = beanId.replaceAll(":", "");
        beanId = beanId.replaceAll("\\.", "");
        beanId = beanId.replaceAll("//", "");

        if (this.applicationContext.containsBean(beanId)) {
            log.warn("#103 bean exists, return, beanId=" + beanId);
            return;
        }

        Integer port = JdbcUtil.getPort(dataNode.getUrl());
        port = null == port ? 3306 : port;

        String password = dataNode.getPassword();
        password = null == password ? "" : password;

        new BeanBuilder() //
                .id(beanId) //
                .parent("abstractCanalInstance") //
                .property("slaveId", 1000 + dataNode.getId()) // 应该不会冲突吧，不会有1000多个从吧
                .property("host", JdbcUtil.getHost(dataNode.getUrl())) //
                .property("port", port + "") //
                .property("username", dataNode.getUsername()) //
                .property("password", password) //
                .ref("canalMessageHandler", canalMessageHandlerId) //
                .registerTo(applicationContext);
    }

    private DataConsumer mailDataConsumer(Pipeline pipeline) {
        DataNode targetDataNode = dataNodeService.findByIdCached(pipeline.getTargetDataNodeId());

        String beanId = "MailDataConsumer-" + targetDataNode.getId() + "-" + targetDataNode.getUrl() + "-" + targetDataNode.getUsername() //
                + "-" + pipeline.getId() //
                + "-" + pipeline.getSourceSchema() + "-" + pipeline.getSourceTable() //
                + "-" + pipeline.getTargetSchema() + "-" + pipeline.getTargetTable();

        DataConsumer dataConsumer = SpringUtil.getBeanCached(this.applicationContext, beanId);
        if (null != dataConsumer) {
            return dataConsumer;
        }

        return (DataConsumer) new BeanBuilder() //
                .id(beanId) //
                .type(MailDataConsumer.class) //
                .propertyValue("pipeline", pipeline) //
                .registerTo(this.applicationContext) //
                .get();
    }

    private DataConsumer jdbcDataConsumer(Pipeline pipeline) {
        DataNode targetDataNode = dataNodeService.findByIdCached(pipeline.getTargetDataNodeId());

        Class<?> type = JdbcDataConsumer.class;
        if (targetDataNode.getUrl().startsWith("jdbc:mysql://")) {
            type = MysqlDataConsumer.class;
        }

        String beanId = type.getSimpleName() + "-" + targetDataNode.getUrl() + "-" + targetDataNode.getUsername() //
                + "-" + pipeline.getId() + "-" + pipeline.getSourceSchema() + "-" + pipeline.getSourceTable() //
                + "-" + pipeline.getTargetSchema() + "-" + pipeline.getTargetTable();

        DataConsumer dataConsumer = SpringUtil.getBeanCached(this.applicationContext, beanId);
        if (null != dataConsumer) {
            return dataConsumer;
        }

        return (DataConsumer) new BeanBuilder() //
                .id(beanId) //
                .type(type) //
                .propertyValue("pipeline", pipeline) //
                .registerTo(this.applicationContext) //
                .get();
    }

    private String asyncRepository(DataNode targetDataNode) {
        if (targetDataNode.getUrl().startsWith("async:kafka://")) {
            return this.kafkaAsyncRepository(targetDataNode);
        }

        if (targetDataNode.getUrl().startsWith("async:activemq://")) {
            return this.activemqAsyncRepository(targetDataNode);
        }

        throw new RuntimeException("#139 targetDataNode=" + targetDataNode);
    }

    private String kafkaAsyncRepository(DataNode targetDataNode) {
        String beanId = "KafkaAsyncRepository-" + targetDataNode.getId() + "-" + targetDataNode.getUrl();

        if (this.applicationContext.containsBean(beanId)) {
            log.warn("#169 bean exists, return, beanId=" + beanId);
            return beanId;
        }

        String kafkaUrl = targetDataNode.getUrl().substring(14);
        Map<String, Object> kafkaProperties = MapUtil.stringObjectMap("bootstrap.servers", kafkaUrl);

        // TODO 给KafkaAsyncRepository一个脚本进去，处理消息内容至老版本的格式
        new BeanBuilder() //
                .id(beanId) //
                .type(KafkaAsyncRepository.class) //
                .property("properties", kafkaProperties) //
                .registerTo(this.applicationContext);

        return beanId;
    }

    private String activemqAsyncRepository(DataNode dataNode) {
        String beanId = "ActiveMQAsyncRepository-" + dataNode.getId() + "-" + dataNode.getUrl();

        if (this.applicationContext.containsBean(beanId)) {
            log.warn("#188 bean exists, return, beanId=" + beanId);
            return beanId;
        }

        String username = dataNode.getUsername();
        String password = dataNode.getPassword();
        String brokerUrl = "tcp://" + dataNode.getUrl().substring(17);

        new BeanBuilder() //
                .id(beanId) //
                .type(ActiveMQAsyncRepository.class) //
                .property("connectionFactory", new ActiveMQConnectionFactory(username, password, brokerUrl)) //
                .registerTo(this.applicationContext) //
                .get();

        return beanId;
    }

    /**
     * 收消息
     */
    public void asyncDataProducer(DataNode dataNode, Pipeline pipeline) {
        String beanId = "asyncDataProducer-" + dataNode.getId() + "-" + pipeline.getId() + "-" + dataNode.getUrl();

        if (this.applicationContext.containsBean(beanId)) {
            log.warn("#212 bean exists, return, beanId=" + beanId);
            return;
        }

        String asyncRepositoryBeanName = this.asyncRepository(dataNode);
        String destinationName = PipelineUtil.destinationName(pipeline); // 如果出现两个Pipeline的收消息的destinationName相同如何处理

        new BeanBuilder() //
                .id(beanId) //
                .type(BudoAsyncServiceBean.class) //
                .property("interfaceType", DataConsumer.class) //
                .property("asyncRepositoryBeanName", asyncRepositoryBeanName) //
                .property("destinationName", destinationName) //
                .ref("instance", "dispatcherDataConsumer") // 收到消息后由dispatcherDataConsumer处理
                .registerTo(this.applicationContext) //
                .get();

        log.info("#149 asyncRepositoryBeanName=" + asyncRepositoryBeanName);
    }

    /**
     * 发消息
     */
    private DataConsumer asyncDataConsumer(Pipeline pipeline) {
        DataNode targetDataNode = dataNodeService.findByIdCached(pipeline.getTargetDataNodeId());

        String beanId = "asyncDataConsumer-" + targetDataNode.getId() + "-" + targetDataNode.getUrl() //
                + "-" + pipeline.getId() + "-" + pipeline.getSourceTable() + "-" + pipeline.getTargetTable();

        DataConsumer dataConsumer = SpringUtil.getBeanCached(this.applicationContext, beanId);
        if (null != dataConsumer) {
            log.warn("#241 bean exists, return, beanId=" + beanId);
            return dataConsumer;
        }

        String asyncRepositoryBeanName = this.asyncRepository(targetDataNode);
        String destinationName = PipelineUtil.destinationName(pipeline);

        return (DataConsumer) new BeanBuilder() //
                .id(beanId) //
                .type(BudoAsyncReferenceBean.class) //
                .property("interfaceType", DataConsumer.class) //
                .property("asyncRepositoryBeanName", asyncRepositoryBeanName) //
                .property("destinationName", destinationName) //
                .registerTo(this.applicationContext) //
                .get();
    }
}