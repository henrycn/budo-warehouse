package org.budo.warehouse.logic.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.budo.support.java.regex.util.RegexUtil;
import org.budo.support.lang.util.StringUtil;
import org.budo.support.spring.expression.util.SpelUtil;
import org.budo.warehouse.logic.api.DataEntry;
import org.budo.warehouse.logic.api.DataMessage;
import org.budo.warehouse.logic.api.IEventFilterLogic;
import org.budo.warehouse.logic.producer.DataMessageImpl;
import org.budo.warehouse.service.entity.Pipeline;
import org.springframework.stereotype.Service;

/**
 * @author lmw
 */
@Service
public class EventFilterLogicImpl implements IEventFilterLogic {
    @Override
    public DataMessage filter(Pipeline pipeline, DataMessage dataMessage) {
        Integer dataNodeId = dataMessage.getDataNodeId();
        List<DataEntry> dataEntries = dataMessage.getDataEntries();

        List<DataEntry> filteredDataEntries = new ArrayList<DataEntry>();
        for (DataEntry dataEntry : dataEntries) {
            boolean filterEntry = this.filterEntry(pipeline, dataEntry);
            if (filterEntry) {
                filteredDataEntries.add(dataEntry);
            }
        }

        return new DataMessageImpl(dataNodeId, filteredDataEntries);
    }

    private boolean filterEntry(Pipeline pipeline, DataEntry dataEntry) {
        return this.bySchema(pipeline, dataEntry) //
                && this.byTable(pipeline, dataEntry) //
                && this.byEventFilter(pipeline, dataEntry);
    }

    /**
     * 库名正则筛选
     */
    private boolean bySchema(Pipeline pipeline, DataEntry dataEntry) {
        String sourceSchema = pipeline.getSourceSchema();
        if (StringUtil.isEmpty(sourceSchema)) {
            return true; // 未配置就全部
        }

        String schemaName = dataEntry.getSchemaName();
        Boolean matches = RegexUtil.matches(sourceSchema, schemaName);
        return matches;
    }

    /**
     * 表名正则筛选
     */
    private boolean byTable(Pipeline pipeline, DataEntry dataEntry) {
        String sourceTable = pipeline.getSourceTable();
        if (StringUtil.isEmpty(sourceTable)) {
            return true; // 未配置就全部
        }

        String tableName = dataEntry.getTableName();
        Boolean matches = RegexUtil.matches(sourceTable, tableName);
        return matches;
    }

    /**
     * 类型等,SPEL筛选
     */
    private boolean byEventFilter(Pipeline pipeline, DataEntry dataEntry) {
        String eventFilter = pipeline.getEventFilter();
        if (StringUtil.isEmpty(eventFilter)) {
            return true; // 未配置就全部
        }

        if (!eventFilter.startsWith("#{")) {
            throw new IllegalArgumentException("#25 eventFilter=" + eventFilter + ", pipeline=" + pipeline);
        }

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("eventType", dataEntry.getEventType());
        map.put("schemaName", dataEntry.getSchemaName());
        map.put("tableName", dataEntry.getTableName());

        String merge = SpelUtil.merge(eventFilter, map);
        return "true".equals(merge);
    }
}