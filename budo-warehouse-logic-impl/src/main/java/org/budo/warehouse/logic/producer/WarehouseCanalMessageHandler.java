package org.budo.warehouse.logic.producer;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.budo.canal.message.handler.DefaultCanalMessageHandler;
import org.budo.warehouse.logic.api.DataConsumer;
import org.budo.warehouse.logic.api.DataEntry;
import org.budo.warehouse.logic.api.DataMessage;

import com.alibaba.otter.canal.protocol.CanalEntry.Entry;

import lombok.Getter;
import lombok.Setter;

/**
 * @author limingwei
 */
@Getter
@Setter
public class WarehouseCanalMessageHandler extends DefaultCanalMessageHandler {
    @Resource(name = "dispatcherDataConsumer")
    private DataConsumer dataConsumer;

    private Integer dataNodeId;

    @Override
    protected void handleEntries(List<Entry> entries) {
        List<DataEntry> dataEntries = new ArrayList<DataEntry>();
        for (Entry entry : entries) {
            CanalDataEntry canalDataEntry = new CanalDataEntry(entry);
            dataEntries.add(canalDataEntry);
        }

        DataMessage dataMessage = new DataMessageImpl(this.getDataNodeId(), dataEntries);
        dataConsumer.consume(dataMessage);
    }
}