package org.budo.warehouse.logic.producer;

import java.util.List;

import org.budo.warehouse.logic.api.DataEntry;
import org.budo.warehouse.logic.api.DataMessage;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author lmw
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class DataMessageImpl implements DataMessage {
    /**
     * sourceDataNodeId
     */
    Integer dataNodeId;

    List<DataEntry> dataEntries;
}