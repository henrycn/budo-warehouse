package org.budo.warehouse.logic.api;

import java.util.List;

/**
 * @author lmw
 */
public interface DataMessage {
    Integer getDataNodeId();

    List<DataEntry> getDataEntries();
}