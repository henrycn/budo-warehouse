package org.budo.warehouse.dao.api;

import java.sql.Timestamp;
import java.util.List;

import org.budo.support.dao.page.Page;
import org.budo.warehouse.service.entity.MailBuffer;

/**
 * @author limingwei
 */
public interface IMailBufferDao {
    Integer insert(MailBuffer mailBuffer);

    Integer countNotSentByPipelineId(Integer pipelineId);

    List<MailBuffer> listNotSentByPipelineId(Integer pipelineId, Page page);

    Timestamp findNotSentMaxCreatedAtByPipelineId(Integer pipelineId);

    Integer updateSentAtByPipelineId(Timestamp sentAt, Integer pipelineId);
}