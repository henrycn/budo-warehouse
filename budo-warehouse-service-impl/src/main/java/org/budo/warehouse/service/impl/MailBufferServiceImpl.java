package org.budo.warehouse.service.impl;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;

import org.budo.support.dao.page.Page;
import org.budo.warehouse.dao.api.IMailBufferDao;
import org.budo.warehouse.service.api.IMailBufferService;
import org.budo.warehouse.service.entity.MailBuffer;
import org.springframework.stereotype.Service;

/**
 * @author limingwei
 */
@Service
public class MailBufferServiceImpl implements IMailBufferService {
    @Resource
    private IMailBufferDao mailBufferDao;

    @Override
    public Integer insert(MailBuffer mailBuffer) {
        return mailBufferDao.insert(mailBuffer);
    }

    @Override
    public Integer countNotSentByPipelineId(Integer pipelineId) {
        return mailBufferDao.countNotSentByPipelineId(pipelineId);
    }

    @Override
    public List<MailBuffer> listNotSentByPipelineId(Integer pipelineId, Page page) {
        return mailBufferDao.listNotSentByPipelineId(pipelineId, page);
    }

    @Override
    public Timestamp findNotSentMaxCreatedAtByPipelineId(Integer pipelineId) {
        return mailBufferDao.findNotSentMaxCreatedAtByPipelineId(pipelineId);
    }

    @Override
    public Integer updateSentAtByPipelineId(Timestamp sentAt, Integer pipelineId) {
        return mailBufferDao.updateSentAtByPipelineId(sentAt,pipelineId);
    }
}