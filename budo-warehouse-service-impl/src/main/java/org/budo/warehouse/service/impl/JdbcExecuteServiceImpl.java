package org.budo.warehouse.service.impl;

import java.util.Arrays;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.budo.graph.annotation.SpringGraph;
import org.budo.support.javax.sql.util.JdbcUtil;
import org.budo.warehouse.service.api.IDataNodeService;
import org.budo.warehouse.service.api.JdbcExecuteService;
import org.budo.warehouse.service.bean.ServiceDynamicBeanProvider;
import org.budo.warehouse.service.entity.DataNode;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * @author lmw
 */
@Slf4j
@Service("jdbcExecuteServiceImpl")
public class JdbcExecuteServiceImpl implements JdbcExecuteService {
    @Resource
    private ServiceDynamicBeanProvider serviceDynamicBeanProvider;

    @Resource
    private IDataNodeService dataNodeService;

    @SpringGraph
    @Override
    public Integer executeUpdate(Integer dataNodeId, String sql, Object[] parameters) {
        DataNode dataNode = dataNodeService.findByIdCached(dataNodeId);
        DataSource dataSource = serviceDynamicBeanProvider.dataSource(dataNode);

        Integer updateCount = JdbcUtil.executeUpdate(dataSource, sql, parameters);

        if (null == updateCount || updateCount < 1) {
            log.warn("#56 sql=" + sql + ", parameters=" + Arrays.toString(parameters) + ", updateCount=" + updateCount);
        }

        return updateCount;
    }
}