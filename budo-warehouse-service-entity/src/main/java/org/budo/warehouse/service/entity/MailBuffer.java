package org.budo.warehouse.service.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 为了控制邮件发送频率,将相关内容写入数据库,适当时机再发送
 * 
 * @author limingwei
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "t_mail_buffer")
public class MailBuffer implements Serializable {
    private static final long serialVersionUID = 7011920206255705181L;

    public MailBuffer(Integer pipelineId, String dataMessage) {
        this.setPipelineId(pipelineId);
        this.setDataMessage(dataMessage);
        this.setCreatedAt(new Timestamp(System.currentTimeMillis()));
    }

    @Id
    @GeneratedValue
    private Integer id;

    @Column(name = "pipeline_id")
    private Integer pipelineId;

    @Column(name = "data_message")
    private String dataMessage;

    @Column(name = "created_at")
    private Timestamp createdAt;

    @Column(name = "sent_at")
    private Timestamp sentAt;
}