package org.budo.warehouse.service.api;

import java.sql.Timestamp;
import java.util.List;

import org.budo.support.dao.page.Page;
import org.budo.warehouse.service.entity.MailBuffer;

/**
 * @author limingwei
 */
public interface IMailBufferService {
    Integer insert(MailBuffer mailBuffer);

    Integer countNotSentByPipelineId(Integer pipelineId);

    List<MailBuffer> listNotSentByPipelineId(Integer pipelineId, Page page);

    Timestamp findNotSentMaxCreatedAtByPipelineId(Integer pipelineId);

    Integer updateSentAtByPipelineId(Timestamp timestamp, Integer pipelineId);
}